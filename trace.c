#define _GNU_SOURCE

#include <errno.h>
#include <fcntl.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MARK(__msg) write(marker_fd, __msg, strlen(__msg))
#define O_AIOS 040000000

/* AIOS */
// #define AIOS
/* READ */
// #define READ
/* File System */
#define BTRFS
/* File Size */
#define SIZE 4096 * 1
/* Test Dir PATH */
#define BTRFS_PATH "/mnt/btrfs_dir/sample.txt"
#define EXT4_PATH "/mnt/ext4_dir/sample.txt"

int main() {
  cpu_set_t mask;
  CPU_ZERO(&mask);

  int marker_fd;
  int file_fd;

  marker_fd = open("/sys/kernel/debug/tracing/trace_marker", O_RDWR);

#ifdef BTRFS
  char file_path[] = BTRFS_PATH;
#else
  char file_path[] = EXT4_PATH;
#endif

#ifdef AIOS
  file_fd = open(file_path, O_RDWR | O_AIOS | O_TRUNC | O_CREAT);
#else
  file_fd = open(file_path, O_RDWR | O_TRUNC | O_CREAT);
#endif

  char buffer[SIZE];
  memset(buffer, '3', SIZE);
  MARK("##### before write\n");
  for(int i=0; i<10; i++)
	write(file_fd, buffer, SIZE);
#ifdef READ
  MARK("##### before read\n");
  read(file_fd, buffer, SIZE);
  MARK("##### after read\n");
#else
  MARK("##### before fsync\n");
  fsync(file_fd);
  MARK("##### after fsync\n");
#endif
  MARK("##### before write2\n");
  write(file_fd, buffer, SIZE);
#ifdef READ
  MARK("##### before read2\n");
  read(file_fd, buffer, SIZE);
  MARK("##### after read2\n");
#else
  MARK("##### before fsync2\n");
  fsync(file_fd);
  MARK("##### after fsync2\n");
#endif
  close(marker_fd);
  close(file_fd);

  return 0;
}
