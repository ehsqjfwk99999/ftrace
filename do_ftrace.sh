#! /bin/bash

# Settings.
if_drop_cache=1
set_one_core=1
btrfs_file_path="/mnt/btrfs_dir/sample.txt"
ext4_file_path="/mnt/ext4_dir/sample.txt"

# Check if root.
echo
read -p "Login as root ? (yes) " input
if [ ${input:-yes} == "n" ] || [ ${input:-yes} == "no" ]; then
    echo -e "Login as root first... Exit\n"
    exit
fi

# Check mount.
read -p "Is mount complete ? (yes) " input
if [ ${input:-yes} == n ] || [ ${input:-yes} == no ]; then
    echo -e "Please mount first... Exit\n"
    exit
fi

# Create or empty file.
echo -n "Empty file... "
echo > ${btrfs_file_path}
echo > ${ext4_file_path}
echo "Done"

# Drop cache.
if [ ${if_drop_cache} -eq 1 ]; then
    echo -n "Drop cache... "
    echo 3 > /proc/sys/vm/drop_caches
    echo "Done"
fi

# Set ftrace.
echo -n "Set ftrace... "
ftrace_fs="/sys/kernel/debug/tracing"
echo 0 > ${ftrace_fs}/tracing_on
############### Ftrace Options ###############
echo function_graph > ${ftrace_fs}/current_tracer
#echo function > ${ftrace_fs}/current_tracer
#echo btrfs_search_slot > ${ftrace_fs}/set_ftrace_filter
#echo btrfs_cow_block > ${ftrace_fs}/set_ftrace_filter
echo > ${ftrace_fs}/set_ftrace_filter
#echo 1 > ${ftrace_fs}/options/func_stack_trace
echo 0 > ${ftrace_fs}/options/func_stack_trace
##############################################
gcc -o trace trace.c
echo "Done"

# Disable core if ${set_one_core} is set.
if [ ${set_one_core} -eq 1 ]; then
    echo -n "Disable cores..."
    chcpu -d 1-7
    echo "Done"
fi

# Ttrace kernel.
echo -n "Trace kernel... "
echo 1 > ${ftrace_fs}/tracing_on
taskset -c 0 ./trace start
sleep 40
echo 0 > ${ftrace_fs}/tracing_on
echo "Done"

# Enable core if ${set_one_core} is set.
if [ ${set_one_core} -eq 1 ]; then
    echo -n "Enable cores..."
    chcpu -e 1-7
    echo "Done"
fi

# Moving log.
echo -n "Move log ... "
cat ${ftrace_fs}/per_cpu/cpu0/trace_pipe > ./log.txt
echo "Done"

# Clean up.
echo -n "Cleaning up... "
rm trace
echo "Done"

echo -e "Trace Complete!\n"
